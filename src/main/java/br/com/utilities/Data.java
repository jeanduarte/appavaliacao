package br.com.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Data {

	String dataAtual;
	
	public Data() {
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		this.dataAtual = dateFormat.format(date);
		
	}
	
	public String get() {
		
		return this.dataAtual;
	}
}
