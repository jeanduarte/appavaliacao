package br.com.appAvalicaoUnicap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppAvalicaoUnicapApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppAvalicaoUnicapApplication.class, args);
	}
	

}

