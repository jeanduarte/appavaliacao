/**
 * 
 */
package br.com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import br.com.utilities.Data;

/**
 * @author jean
 *
 */
@Entity
public class Comentario {

	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer id;
	
	@Column(name="autor")
	public String autor;
	
	@Column(name="mensagem")
	public String mensagem;
	
	@Column(name="data_criacao")
	public String dataCriacao;
	
	@Column(name="pontuacao")
	public Integer pontuacao;
	
	
	/*
	 * @Param String, String, Date
	 * */
	public Comentario( String autor, String mensagem ) {

		this.autor = autor;
		this.mensagem = mensagem;
		this.dataCriacao = new Data().get();
		this.pontuacao = 0;
	}
	

	public Integer getId() {
		return this.id;
	}


	public String getAutor() {
		return autor;
	}

	public String getMensagem() {
		return mensagem;
	}

	public String getDataCriacao() {
		return dataCriacao;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}
	
	

}
