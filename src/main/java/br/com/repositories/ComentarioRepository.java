/**
 * 
 */
package br.com.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.models.Comentario;
/**
 * @author jean
 *
 */

@Repository
public interface ComentarioRepository extends CrudRepository < Comentario, Integer > {

	
	Comentario findBy_id( Integer id );
}

