
package br.com.controllers;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.repositories.ComentarioRepository;
import br.com.models.*;


/**
 * @author jean
 *
 */

@Controller
@RequestMapping("/comentarios")
public class ComentarioController {

	@Autowired
	private ComentarioRepository repository;
	
	
	@GetMapping( value = "/all" )
	public @ResponseBody Iterable<Comentario> getAll() {
		
		return repository.findAll();
	}
	
	
	@GetMapping( value = "/find/{id}" )
	public Comentario getById( @PathVariable("id") Integer id ) {
		
		return repository.findBy_id( id );
	}
	

	@PutMapping( value = "/update/{id}" )
	public void update( @PathVariable("id") Integer id, @Valid @RequestBody Comentario comentario ) {
			 
		repository.save( comentario ); // uma vez vinculado podemos atualizar o registro daquele ID
	}
	
	
	@PostMapping( value = "/add" )
	public Comentario createNew( @RequestParam String autor, @RequestParam String mensagem, @RequestParam Date dataPostagem) {
		
		Comentario comentario = new Comentario( autor, mensagem );
		repository.save(comentario);
		return comentario;
	}

	
	@DeleteMapping( value = "/delete/{id}" )
	public void deletePet(@PathVariable Integer id) {
		
		repository.deleteById( id );	
	}
	
}
